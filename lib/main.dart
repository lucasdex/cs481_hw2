import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Widget textSection = Container(
  padding: EdgeInsets.all(20),
  child: Text(
      "This a random text where I can describe a little the picture you can see on the top of your screen"
      "So actually it's the vue point from Valberg a France mountain"
      "I have passed my summer's holidays with some friends"
      "we passed 2 weeks for a road trip in France and we passed trough Valberg !"
      "If you like my application you can leave a like... when I will handle the click button"
      "which is not the case yet we are not supposed to handle that in this exercise."
      "Enjoy the app even if it's not yet responsive !"),
);

Column _buttonColumn(Color color, IconData icon, String label) {
  return Column(
    children: [
      Icon(icon, color: color),
      Container(
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

Widget buttonRow = Container(
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      _buttonColumn(Colors.blue, Icons.phone, "Call"),
      _buttonColumn(Colors.blue, Icons.message, "Text"),
      _buttonColumn(Colors.blue, Icons.share, "Share")
    ],
  ),
);

Widget title = Container(
  padding: EdgeInsets.all(31),
  child: Row(
    children: [
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 8),
              child: Text(
                'RoadTrip Summer 2020',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              'looks pretty awesome right ?',
              style: TextStyle(color: Colors.grey[500]),
            )
          ],
        ),
      ),
      Icon(
        Icons.favorite,
        color: Colors.red[500],
      ),
      Text('42')
    ],
  ),
);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout homework'),
        ),
        body: Column(
          children: [
            Image.asset(
              'images/rt_valberg.jpeg',
              width: 600,
              height: 200,
              fit: BoxFit.fitWidth,
              alignment: FractionalOffset.bottomCenter,
            ),
            title,
            buttonRow,
            textSection
          ],
        ),
      ),
    );
  }
}
